#UDPPingerServer.py
# We will need the following module to generate randmoized lost packets
import random
from socket import *
import time

print 'Running...'

serverName = '127.0.0.1'

# Create a UDP socket
clientSocket = socket(AF_INET, SOCK_DGRAM)

# Set our timeout
clientSocket.settimeout(1)

# Know which sequence number is being used
sequence_num = 0

while sequence_num < 10:
   # Our message to send
   message = 'Ping'
   
   # Track our start time
   starttime = time.time()
   
   # Use port 8000 to send the server our message
   clientSocket.sendto(message, (serverName, 8000))
   
   try:
      # Receive a message from the server
      message, adr = clientSocket.recvfrom(1024)
      
      # Find out how much time has elapsed since starting to send message to receiving
      elapsedtime = (time.time() - starttime)
      
      print sequence_num
      print message
      
      print 'RTT: ', str(elapsedtime), ' seconds'
      
   except timeout:# If the socket takes over a second then consider the sequence_num packet timed out
      print sequence_num
      print 'The Request Timed Out'
      
      
   # Increase the sequence number
   sequence_num += 1

   if sequence_num > 10: # Only send 10 Packets
      clientSocket.close()












