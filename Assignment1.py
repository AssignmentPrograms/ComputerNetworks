#import socket module
from socket import *                                   

#Prepare a sever socket 
#serverName = 'Brandon\'s Server'
serverPort = 7005
#HOST = gethostname()


#Create server socket
serverSocket = socket(AF_INET, SOCK_STREAM)
#serverSocket.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
serverSocket.bind(('', serverPort))
serverSocket.listen(1)
print 'I am listening on port: ', serverPort

while True: 
    #Establish the connection 
    print 'Ready to serve...'
    connectionSocket, addr =  serverSocket.accept()
    

    try:
	#Retrieve the message through the connectionSocket (max of 1024 bytes)
        message = connectionSocket.recv(1024)
	#capitalizedSentence = message.upper()
	
	print message, message.split()[0], ':', message.split()[1]
	
	#Send the capitalizedSentence through the connectionSocket
	connectionSocket.send(message)


        filename = message.split()[1]                  
        #print filename, '::', filename[1:]
	
	f = open(filename[1:])                        
        outputdata = f.read()
        #print outputdata

        #Send one HTTP header line into socket 
	connectionSocket.send('\nHTTP/1.1 200 OK\n\n')
	connectionSocket.send(outputdata)
        
        #Send the content of the requested file to the client
        #for i in range(0, len(outputdata)):            
        #	connectionSocket.send(outputdata[i])
        connectionSocket.close()

    except IOError: 
        #Send response message for file not found
	print '404 Not Found' 
        connectionSocket.send('\nHTTP/1.1 404 Not Found\n\n')	 
	
	#Close client socket
	connectionSocket.close()     



